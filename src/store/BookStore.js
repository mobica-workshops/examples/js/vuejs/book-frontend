import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import BookService from '@/services/BookService'

export const useBookStore = defineStore('BookStore', () => {
  const booksList = ref([])
  const booksTotal = ref(0)
  const book = ref({})

  const currentBooksCount = computed(() => {
    return booksList.value.length
  })

  function fetchBooks() {
    return BookService.getBooks()
      .then((response) => {
        this.booksTotal = response.data.content.count
        this.booksList = response.data.content.results
      })
      .catch((error) => {
        throw error
      })
  }

  return { booksList, booksTotal, book, currentBooksCount, fetchBooks }
})
