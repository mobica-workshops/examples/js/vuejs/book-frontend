import { defineStore } from 'pinia'
import { ref } from 'vue'
import ConfigService from '@/services/ConfigService'

export const useConfigStore = defineStore(
  'ConfigStore',
  () => {
    const configuration = ref({
      apiUrl: ''
    })
    const error = ref('')

    async function loadConfiguration() {
      try {
        if (!configuration.value.apiUrl) {
          const res = await ConfigService.loadConfiguration()
          configuration.value.apiUrl = res.data['api-url']
        }
      } catch (e) {
        console.log(e)
      }
    }
    return { configuration, error, loadConfiguration }
  },
  {
    persist: {
      storage: localStorage
    }
  }
)
